package de.ddkfm.models;

public class TaxCalculation {
	private double income;
	private boolean children;
	private boolean liveInSaxony;
	private boolean region;
	private boolean church;
	public static final boolean REGION_EAST = true;
	public static final boolean REGION_WEST = false;
	private TaxValueTable taxValues;
	
	public TaxCalculation(double income,TaxValueTable taxValues, boolean children, boolean liveInSaxony, boolean region,boolean church) {
		super();
		this.income = income;
		this.children = children;
		this.liveInSaxony = liveInSaxony;
		this.region = region;
		this.taxValues = taxValues;
		this.church = church;
	}
	private double getValue(String key){
		return taxValues.getValue(key);
	}
	public double getIncomeTaxJear(double extra){
		double incomeTax = 0;
		double helpIncome = income;
		income *= 12;
		income += extra;
		if(income >= getValue("section0Border") +1 && income <= getValue("section1Border")){
			double y = Math.ceil(income - getValue("section0Border"))/10000;
			incomeTax = (getValue("section1Factor") * y + getValue("section1Const"))*y;
		}else
			if(income >= getValue("section1Border") +1 && income <= getValue("section2Border")){
				double z = Math.ceil(income - getValue("section1Border"))/10000;
				incomeTax = (getValue("section2Factor") * z + getValue("section2Const"))*z + getValue("section2Const2");
			}else
				if(income >= getValue("section2Border") +1 && income <= getValue("section3Border"))
					incomeTax = getValue("section3Rate") * income - getValue("section3Const");
				else
					if(income >= getValue("section3Border"))
						incomeTax = getValue("section4Rate") * income - getValue("section4Const");
		income = helpIncome;
		return incomeTax;
	}
	public double getIncomeTaxMonth(){
		return getIncomeTaxJear(0.0) / 12;
	}
	public double getChurchTax(){
		return (church)?getIncomeTaxMonth() * getValue("churchTax")*0.01:0;
	}
	public double getSoli(){
		return getIncomeTaxMonth() * getValue("solitaritySurcharge") * 0.01;
	}
	public double getPercent(){
		return getIncomeTaxMonth() / income;
	}
	public double getBorderPercent(){
		return getIncomeTaxJear(1.0) - getIncomeTaxJear(0);
	}
	public boolean isHealthAndCareInsuranceBorderReached(){
		return income >= getValue("healthEarningBorder");
	}
	private double getPensionBorder(){
		return region?getValue("pensionEarningBorderEast"):getValue("pensionEarningBorderWest");
	}
	public boolean isPensionAndUnemploymentBorderReached(){
		return income >= getPensionBorder();
	}
	public double getHealthInsurance(){
		double helpIncome = !isHealthAndCareInsuranceBorderReached()?income:getValue("healthEarningBorder");
		double percentage = (0.5 * getValue("healthInsurance") + getValue("healthExtraAmount"))*0.01;
		return helpIncome * percentage;
	}
	public double getPensionInsurance(){
		double helpIncome = !isPensionAndUnemploymentBorderReached()?income:getPensionBorder();
		double percentage = 0.5 * getValue("pensionInsurance") * 0.01;
		return helpIncome * percentage;
	}
	public double getUnemploymentInsurance(){
		double helpIncome = !isPensionAndUnemploymentBorderReached()?income:getPensionBorder();
		double percentage = 0.5 * getValue("unemploymentInsurance") * 0.01;
		return helpIncome * percentage;
	}
	public double getCareInsurance(){
		double helpIncome = !isHealthAndCareInsuranceBorderReached()?income:getValue("healthEarningBorder");
		double percentage = 0.01 * (0.5 * getValue("careInsurance") 
								+ (children?getValue("withoutChildren"):0.0) 
								+ (liveInSaxony?getValue("saxonyExtraAmount"):0.0));
		return helpIncome * percentage;
	}
	public double getNet(){
		return income - getIncomeTaxMonth() 
					  - getSoli() 
					  - (church?getChurchTax():0)
					  - getHealthInsurance()
					  - getPensionInsurance()
					  - getUnemploymentInsurance()
					  - getCareInsurance();
	}
	public double getNetPercent(){
		return getNet() / income;
	}
	
}
