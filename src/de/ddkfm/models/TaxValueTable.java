package de.ddkfm.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class TaxValueTable {
	private Properties values;

	public TaxValueTable(int section0Border, int section1Border, double section1Factor, double section1Const,
			int section2Border, double section2Factor, double section2Const, double section2Const2, int section3Border,
			double section3Rate, double section3Const, double section4Rate, double section4Const,
			double solitaritySurcharge, double churchTax, double healthInsurance, double healthExtraAmount,
			double healthEarningBorder, double pensionInsurance, double pensionEarningBorderEast,
			double pensionEarningBorderWest, double unemploymentInsurance, double careInsurance,
			double saxonyExtraAmount, double withoutChildren) {
		super();
		values.put("section0Border", section0Border);
		values.put("section1Border", section1Border);
		values.put("section1Factor", section1Factor);
		values.put("section1Const", section1Const);
		values.put("section2Border", section2Border);
		values.put("section2Factor", section2Factor);
		values.put("section2Const", section2Const);
		values.put("section2Const2", section2Const2);
		values.put("section3Border", section3Border);
		values.put("section3Rate", section3Rate);
		values.put("section3Const", section3Const);
		values.put("section4Rate", section4Rate);
		values.put("section4Const", section4Const);
		values.put("solitaritySurcharge", solitaritySurcharge);
		values.put("churchTax", churchTax);
		values.put("healthInsurance", healthInsurance);
		values.put("healthExtraAmount", healthExtraAmount);
		values.put("healthEarningBorder", healthEarningBorder);
		values.put("pensionInsurance", pensionInsurance);
		values.put("pensionEarningBorderEast", pensionEarningBorderEast);
		values.put("pensionEarningBorderWest", pensionEarningBorderWest);
		values.put("unemploymentInsurance", unemploymentInsurance);
		values.put("careInsurance", careInsurance);
		values.put("saxonyExtraAmount", saxonyExtraAmount);
		values.put("withoutChildren", withoutChildren);
	}
	private void loadValues(Properties props){
		values = new Properties(props);
	}
	public double getValue(String key){
		return Double.parseDouble(values.getProperty(key));
	}
	public void setValue(String key, double value){
		values.setProperty(key,Double.toString(value));
	}
	public TaxValueTable(){
		Properties props = new Properties();
		File propertyFile = new File(System.getProperty("user.home") + "/.incomeDemo/TaxValueTable.properties");
		//Loading the Properties-File
		if(propertyFile.exists())
			try {
				props.load(new FileInputStream(propertyFile));
			} catch (FileNotFoundException ex) {
				System.out.println("Properties-Datei nicht gefunden");
			} catch (IOException ex) {
				System.out.println("Fehler beim Lesen der Properties-Datei");
			}
		else{
			try {
				props.load(getClass().getResourceAsStream("DefaultTaxValues.properties"));
			} catch (IOException e) {
				System.out.println("Fehler beim Laden der Default-Wertetabelle");
			}
			try {
				propertyFile.getParentFile().mkdirs();
				propertyFile.createNewFile();
			} catch (IOException e) {
				System.out.println("Fehler beim Erstellen der Properties-Datei");
			}
			try {
				props.store(new FileOutputStream(propertyFile),"");
			} catch (FileNotFoundException e) {
				System.out.println("Properties-Datei existiert nicht");
			} catch (IOException e) {
				System.out.println("Fehler beim Schreiben der Properties-Datei");
			}
		}
		loadValues(props);
	}
	public void storePropertyFile(){
		File propertyFile = new File(System.getProperty("user.home") + "/.incomeDemo/TaxValueTable.properties");
		if(!propertyFile.exists()){
			try {
				propertyFile.getParentFile().mkdirs();
				propertyFile.createNewFile();
			} catch (IOException e) {
				System.out.println("Fehler beim Erstellen der Properties-Datei");
			}
		}
		try {
			values.store(new FileOutputStream(propertyFile),"");
		} catch (FileNotFoundException e) {
			System.out.println("Properties-Datei existiert nicht");
		} catch (IOException e) {
			System.out.println("Fehler beim Schreiben der Properties-Datei");
		}
	}
}
