package de.ddkfm.application;

import java.net.URL;
import java.util.ResourceBundle;

import de.ddkfm.models.TaxValueTable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class ValueEditingController{

    @FXML
    private TextField edSection2Const;

    @FXML
    private TextField edSection1Border;

    @FXML
    private TextField edPV;

    @FXML
    private TextField edRV;

    @FXML
    private TextField edSection3Factor;

    @FXML
    private TextField edRV_BbG_Ost;

    @FXML
    private TextField edSection1Const;

    @FXML
    private TextField edPV_Saxony;

    @FXML
    private TextField edSoli;

    @FXML
    private TextField edKV_BbG;

    @FXML
    private TextField edSection1Factor;

    @FXML
    private TextField edSection2Border;

    @FXML
    private TextField edKV_Extra;

    @FXML
    private TextField edSection2Const2;

    @FXML
    private TextField edSection4Factor;

    @FXML
    private TextField edKV;

    @FXML
    private TextField edRV_BbG_West;

    @FXML
    private TextField edSection4Const;

    @FXML
    private TextField edChurch;

    @FXML
    private TextField edSection0Border;

    @FXML
    private TextField edSection3Border;

    @FXML
    private TextField edSection2Factor;

    @FXML
    private TextField edAV;

    @FXML
    private TextField edSection3Const;

    @FXML
    private TextField edPV_withoutChildren;

    @FXML
    private Button btValueSave;


    @FXML
    void btValueSaveAction(ActionEvent event) {
    	TaxValueTable values = new TaxValueTable();
    	System.out.println(Double.parseDouble(edSection0Border.getText().replace(',', '.')));
    	values.setValue("section0Border", Double.parseDouble(edSection0Border.getText().replace(',','.')));
		values.setValue("section1Border", Double.parseDouble(edSection1Border.getText().replace(',','.')));
		values.setValue("section1Factor", Double.parseDouble(edSection1Factor.getText().replace(',','.')));
		values.setValue("section1Const", Double.parseDouble(edSection1Const.getText().replace(',','.')));
		values.setValue("section2Border", Double.parseDouble(edSection2Border.getText().replace(',','.')));
		values.setValue("section2Factor", Double.parseDouble(edSection2Factor.getText().replace(',','.')));
		values.setValue("section2Const", Double.parseDouble(edSection2Const.getText().replace(',','.')));
		values.setValue("section2Const2", Double.parseDouble(edSection2Const2.getText().replace(',','.')));
		values.setValue("section3Border", Double.parseDouble(edSection3Border.getText().replace(',','.')));
		values.setValue("section3Rate", Double.parseDouble(edSection3Factor.getText().replace(',','.')));
		values.setValue("section3Const", Double.parseDouble(edSection3Const.getText().replace(',','.')));
		values.setValue("section4Rate", Double.parseDouble(edSection4Factor.getText().replace(',','.')));
		values.setValue("section4Const", Double.parseDouble(edSection4Const.getText().replace(',','.')));
		values.setValue("solitaritySurcharge", Double.parseDouble(edSoli.getText().replace(',','.')));
		values.setValue("churchTax", Double.parseDouble(edChurch.getText().replace(',','.')));
		values.setValue("healthInsurance", Double.parseDouble(edKV.getText().replace(',','.')));
		values.setValue("healthExtraAmount", Double.parseDouble(edKV_Extra.getText().replace(',','.')));
		values.setValue("healthEarningBorder", Double.parseDouble(edKV_BbG.getText().replace(',','.')));
		values.setValue("pensionInsurance", Double.parseDouble(edRV.getText().replace(',','.')));
		values.setValue("pensionEarningBorderEast", Double.parseDouble(edRV_BbG_Ost.getText().replace(',','.')));
		values.setValue("pensionEarningBorderWest", Double.parseDouble(edRV_BbG_West.getText().replace(',','.')));
		values.setValue("unemploymentInsurance", Double.parseDouble(edAV.getText().replace(',','.')));
		values.setValue("careInsurance", Double.parseDouble(edPV.getText().replace(',','.')));
		values.setValue("saxonyExtraAmount", Double.parseDouble(edPV_Saxony.getText().replace(',','.')));
		values.setValue("withoutChildren", Double.parseDouble(edPV_withoutChildren.getText().replace(',','.')));
    	values.storePropertyFile();
    }
    @FXML
	public void initialize() {
    	TaxValueTable values = new TaxValueTable();
		edSection0Border.setText(Double.toString(values.getValue("section0Border")).replace('.',','));
		edSection1Border.setText(Double.toString(values.getValue("section1Border")).replace('.',','));
		edSection1Factor.setText(Double.toString(values.getValue("section1Factor")).replace('.',','));
		edSection1Const.setText(Double.toString(values.getValue("section1Const")).replace('.',','));
		edSection2Border.setText(Double.toString(values.getValue("section2Border")).replace('.',','));
		edSection2Factor.setText(Double.toString(values.getValue("section2Factor")).replace('.',','));
		edSection2Const.setText(Double.toString(values.getValue("section2Const")).replace('.',','));
		edSection2Const2.setText(Double.toString(values.getValue("section2Const2")).replace('.',','));
		edSection3Border.setText(Double.toString(values.getValue("section3Border")).replace('.',','));
		edSection3Factor.setText(Double.toString(values.getValue("section3Rate")).replace('.',','));
		edSection3Const.setText(Double.toString(values.getValue("section3Const")).replace('.',','));
		edSection4Factor.setText(Double.toString(values.getValue("section4Rate")).replace('.',','));
		edSection4Const.setText(Double.toString(values.getValue("section4Const")).replace('.',','));
		edSoli.setText(Double.toString(values.getValue("solitaritySurcharge")).replace('.',','));
		edChurch.setText(Double.toString(values.getValue("churchTax")).replace('.',','));
		edKV.setText(Double.toString(values.getValue("healthInsurance")).replace('.',','));
		edKV_Extra.setText(Double.toString(values.getValue("healthExtraAmount")).replace('.',','));
		edKV_BbG.setText(Double.toString(values.getValue("healthEarningBorder")).replace('.',','));
		edRV.setText(Double.toString(values.getValue("pensionInsurance")).replace('.',','));
		edRV_BbG_Ost.setText(Double.toString(values.getValue("pensionEarningBorderEast")).replace('.',','));
		edRV_BbG_West.setText(Double.toString(values.getValue("pensionEarningBorderWest")).replace('.',','));
		edAV.setText(Double.toString(values.getValue("unemploymentInsurance")).replace('.',','));
		edPV.setText(Double.toString(values.getValue("careInsurance")).replace('.',','));
		edPV_Saxony.setText(Double.toString(values.getValue("saxonyExtraAmount")).replace('.',','));
		edPV_withoutChildren.setText(Double.toString(values.getValue("withoutChildren")).replace('.',','));
	}

}
