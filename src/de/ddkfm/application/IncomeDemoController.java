package de.ddkfm.application;

import java.net.URL;
import java.util.ResourceBundle;

import de.ddkfm.models.TaxCalculation;
import de.ddkfm.models.TaxValueTable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class IncomeDemoController implements Initializable{

    @FXML
    private RadioButton rbEast;

    @FXML
    private TextField edAverageRate;

    @FXML
    private TextField edUnemploymentInsurhance;

    @FXML
    private TextField edNet;

    @FXML
    private TextField edCareInsurance;

    @FXML
    private ToggleGroup tgRegion;

    @FXML
    private CheckBox cbLimitUI;

    @FXML
    private TextField edPercentage;

    @FXML
    private CheckBox cbWithoutChildren;

    @FXML
    private TextField edChurchTax;

    @FXML
    private MenuItem miFileTXT;

    @FXML
    private MenuItem miValuesEdit;

    @FXML
    private CheckBox cbChurch;

    @FXML
    private TextField edSoli;

    @FXML
    private CheckBox cbLimitCI;

    @FXML
    private MenuItem miValuesSave;

    @FXML
    private CheckBox cbSaxony;

    @FXML
    private MenuItem miAbout;

    @FXML
    private TextField edBorderRate;

    @FXML
    private RadioButton rbWest;

    @FXML
    private MenuItem miFileCSV;

    @FXML
    private TextField edHealthInsurance;

    @FXML
    private TextField edIncomeTax;

    @FXML
    private TextField edPensionInsurance;

    @FXML
    private LineChart<Number, Number> chartTax;

    @FXML
    private CheckBox cbLimitPI;

    @FXML
    private TextField edIncome;

    @FXML
    private CheckBox cbLimitHI;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Series<Number, Number> taxFunction = new Series<>();
		taxFunction.setName("Steuertarif");
		
		for(int i = 1; i <= 25000 ; i += 700){
			double calc = new TaxCalculation(i, new TaxValueTable(),true,true,true,true).getPercent()*100;
			taxFunction.getData().add(new Data<Number, Number>(i,calc));
		}
		chartTax.getData().add(taxFunction);
		edIncome.textProperty().addListener((a,b,c)->changeInput());
		cbSaxony.selectedProperty().addListener((a,b,c)->changeInput());
		cbWithoutChildren.selectedProperty().addListener((a,b,c)->changeInput());
		tgRegion.selectedToggleProperty().addListener((a,b,c)->changeInput());
		cbChurch.selectedProperty().addListener((a,b,c)->changeInput());
		addActions();
	}
	private void changeInput(){
		double income;
		try {
			income = Double.parseDouble(edIncome.getText());
		} catch (NumberFormatException e) {
			income = -1;
		}
		boolean children = cbWithoutChildren.isSelected();
		boolean liveInSaxony = cbSaxony.isSelected();
		boolean region = rbEast.isSelected();
		boolean church = cbChurch.isSelected();
		if (income != -1) {
			TaxCalculation calc = new TaxCalculation(income, new TaxValueTable(), children, liveInSaxony, region, church);
			Series<Number, Number> currentIncome = new Series<>();
			currentIncome.setName("aktueller Bruttolohn");
			currentIncome.getData().add(new Data<Number, Number>(income,calc.getPercent()*100));
			if(chartTax.getData().size() > 1)
				chartTax.getData().remove(1);
			chartTax.getData().add(currentIncome);
			
			edIncomeTax.setText(String.format("%.2f �", calc.getIncomeTaxMonth()));
			edSoli.setText(String.format("%.2f �", calc.getSoli()));
			edChurchTax.setText(String.format("%.2f �", calc.getChurchTax()));
			edAverageRate.setText(String.format("%.2f %s", calc.getPercent()*100,"%"));
			edBorderRate.setText(String.format("%.2f %s", calc.getBorderPercent()*100,"%"));
			edHealthInsurance.setText(String.format("%.2f �", calc.getHealthInsurance(),"�"));
			cbLimitHI.setSelected(calc.isHealthAndCareInsuranceBorderReached());
			edPensionInsurance.setText(String.format("%.2f �", calc.getPensionInsurance(),"�"));
			cbLimitPI.setSelected(calc.isPensionAndUnemploymentBorderReached());
			edUnemploymentInsurhance.setText(String.format("%.2f �", calc.getUnemploymentInsurance(),"�"));
			cbLimitUI.setSelected(calc.isPensionAndUnemploymentBorderReached());
			edCareInsurance.setText(String.format("%.2f �", calc.getCareInsurance(),"�"));
			cbLimitCI.setSelected(calc.isHealthAndCareInsuranceBorderReached());
			edNet.setText(String.format("%.2f �", calc.getNet(),"�"));
			edPercentage.setText(String.format("%.2f %s", calc.getNetPercent()*100,"%"));
		}
	}
	private void addActions(){
		miValuesEdit.setOnAction(e->{
			Stage stage = new Stage();
			Pane root = null;
			try {
				root = (Pane)FXMLLoader.load(getClass().getResource("ValueEditing.fxml"));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("Werte bearbeiten");
			stage.show();
		});
		miFileTXT.setOnAction(e->{
			
		});
		miFileCSV.setOnAction(e->{
			
		});
		miValuesSave.setOnAction(e->{
			
		});
		miAbout.setOnAction(e->{
			Alert help = new Alert(AlertType.INFORMATION);
			help.setTitle("�ber Steuer-Demo");
			help.setHeaderText("Ist der Lehrer richtig flei�ig, zahlt er 41,36�");
			help.setContentText("Steuer-Demo\n"
					+ "f�r den WLR-Unterricht des Herrn S�nkel\n"
					+ "\n"
					+ "Copyright 2016\n"
					+ "Maximilian Sch�dlich\n"
					+ "maxschaedlich@hotmail.de\n"
					+ "\n"
					+ "Diese Software steht unter der \n"
					+ "GNU General Public Licence 3");
			help.show();
		});
	}

}
